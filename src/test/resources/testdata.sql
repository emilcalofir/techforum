
INSERT INTO CATEGORY (id, name)
VALUES
(11, 'Programming Languages'),
(12, 'Frameworks')
;


INSERT INTO TECHNOLOGY (id, name, category_id)
VALUES
(1111, 'Java', 11),
(1112, 'Kotlin', 11),
(1113, 'TypeScript', 11),
(1114, 'C++', 11),

(1115, 'Spring', 12),
(1116, 'Angular', 12),
(1117, 'React', 12)

;


INSERT INTO TOPIC (id, name, question, technology_id)
VALUES
(1111, 'What is useful in Java 10?', 'From my experience the Java 9 was ....', 1111),
(1112, '“How should I use Optional correctly?', 'this is my way of using the Optional ....', 1111)
;

INSERT INTO ANSWER (id, answer, points, accepted_answer, topic_id, reply_to)
VALUES
(1111, 'My opinion is taht..?', 0, FALSE, 1111, null),
(1112, 'Idontk think..', 0, FALSE, 1111, null),
(1113, 'Youm may be right..', 0, FALSE, 1111, 1111),
(1114, 'Yes you areright..', 0, FALSE, 1111, 1111)
;