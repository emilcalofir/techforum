package com.whitebox.techforum.repository;

import com.whitebox.techforum.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

/**
 * Created by emil on 9/10/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    UserRepository repository;

    @Test
    public void findByUsername() throws Exception {
        User user = repository.findByUsername("admin@tech");

        assertEquals("admin@tech", user.getUsername());
    }

    @Test
    public void findByUsername2() throws Exception {
        User user = repository.findByUsername("user1@tech");

        assertEquals("user1@tech", user.getUsername());
    }

}