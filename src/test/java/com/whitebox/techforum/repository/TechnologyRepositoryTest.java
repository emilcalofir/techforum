package com.whitebox.techforum.repository;

import com.whitebox.techforum.model.Category;
import com.whitebox.techforum.model.Technology;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by emil on 9/5/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TechnologyRepositoryTest {

    @Autowired
    TechnologyRepository repository;

    @Autowired
    CategoryRepository categoryRepository;

    @Test
    public void test_repository(){

        Iterable<Technology> persistedCat = repository.findAll();

        Technology cat = persistedCat.iterator().next();
        assertNotNull(cat);
    }

    @Test
    public void test_save(){

        Optional<Category> cat = categoryRepository.findById(1);

        Technology tech = Technology.builder().name("new Technology").categoryId(cat.get().getId()).build();
        repository.save(tech);

        Optional<Technology> persistedTech = repository.findById(tech.getId());

        assertNotEquals(tech, persistedTech);
        assertEquals(tech.getId(), persistedTech.get().getId());
    }

}