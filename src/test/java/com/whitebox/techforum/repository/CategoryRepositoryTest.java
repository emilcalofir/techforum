package com.whitebox.techforum.repository;

import com.whitebox.techforum.model.Category;
import com.whitebox.techforum.model.Technology;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by emil on 9/4/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CategoryRepositoryTest {

    @Autowired
    CategoryRepository repository;

    @Test
    public void test_repository(){

        Iterable<Category> persistedCat = repository.findAll();
        Category cat = persistedCat.iterator().next();
        List<Technology> techs = cat.getTechnologies();

        assertNotNull(cat);
        assertTrue(techs.size()>0);
    }



    @Test
    public void test_save(){

        Category cat = Category.builder().name("new cat").build();
        repository.save(cat);

        Optional<Category> persistedCat = repository.findById(cat.getId());

        assertNotEquals(cat, persistedCat);
        assertEquals(cat.getId(), persistedCat.get().getId());
    }

}