package com.whitebox.techforum.repository;

import com.whitebox.techforum.model.Answer;
import com.whitebox.techforum.model.Topic;
import com.whitebox.techforum.model.dto.AnswerDTO;
import com.whitebox.techforum.model.dto.MyAnswersDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by emil on 9/6/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AnswerRepositoryTest {



    @Autowired
    AnswerRepository repository;

    @Autowired
    TopicRepository topicRepository;

    @Test
    public void test_repository(){

        Iterable<Answer> persistedAnswers = repository.findAll();
        Answer ans = persistedAnswers.iterator().next();

        assertNotNull(ans);
    }


    @Test
    public void test_save(){

        Optional<Topic> topic = topicRepository.findById(1);

        Answer answer = Answer.builder().answer("You think that , i disagree").points(0).topicId(topic.get().getId()).build();
        repository.save(answer);

        Optional<Answer> persistedAnswer = repository.findById(answer.getId());

        assertNotEquals(answer, persistedAnswer);
        assertEquals(answer.getId(), persistedAnswer.get().getId());
    }


    @Test
    public void findByAnswer() throws Exception {
        List<AnswerDTO> rez = repository.findByAnswer("YEs", 2, PageRequest.of(0, 5, Sort.by("points")));

        assertTrue(rez.size() > 0);
    }


    @Test
    public void findMyMy() throws Exception {
        List<MyAnswersDTO> rez = repository.findMyMy("a", 2);

        assertTrue(rez.size() > 0);
    }

}