package com.whitebox.techforum.repository;

import com.whitebox.techforum.model.Technology;
import com.whitebox.techforum.model.Topic;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by emil on 9/6/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TopicRepositoryTest {

    @Autowired
    TopicRepository repository;

    @Autowired
    TechnologyRepository technologyRepository;

    @Test
    public void test_repository(){
        Iterable<Topic> persistedTopics= repository.findAll();
        Topic cat = persistedTopics.iterator().next();

        assertNotNull(cat);
    }

    @Test
    public void test_save(){

        Optional<Technology> tech = technologyRepository.findById(1);

        Topic topic = Topic.builder().name("Whasapp doc?").question("I cant find the missing wabbit").technologyId(tech.get().getId()).build();
        repository.save(topic);

        Optional<Topic> persistedTopic = repository.findById(topic.getId());

        assertNotEquals(topic, persistedTopic);
        assertEquals(topic.getId(), persistedTopic.get().getId());
    }

}