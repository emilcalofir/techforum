package com.whitebox.techforum.config;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by emil on 9/4/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class JpaDatasourceTest {

    @Autowired
    JpaDatasource datasource;

    @Value("${spring.datasource.username}")
    String username;

    @Test
    public void test_datasource(){
        assertNotNull(datasource);
        assertEquals(username, datasource.username);
    }
}