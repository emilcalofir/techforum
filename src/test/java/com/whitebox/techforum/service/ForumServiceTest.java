package com.whitebox.techforum.service;

import com.whitebox.techforum.exception.TopicNotFoundException;
import com.whitebox.techforum.model.Topic;
import com.whitebox.techforum.model.dto.AddTopicDTO;
import com.whitebox.techforum.model.dto.AnswerDTO;
import com.whitebox.techforum.repository.TopicRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by emil on 9/6/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
//@Sql("classpath:testdata.sql")
public class ForumServiceTest {


    @Autowired
    ForumService service;

    @Autowired
    TopicRepository topicRepository;

    @Test
    public void listTechnologies() throws Exception {

        List<AnswerDTO> list = (List<AnswerDTO>) service.getAnswersForTopics(1);

        log.info(Arrays.asList(list).toString());

        assertTrue(list.size() > 0);
    }

    @Test(expected = TopicNotFoundException.class)
    public void deleteTopic() throws Exception {

        Topic topic = service.createTopic(AddTopicDTO.builder().name("test").question("nameeee?").technologyId(1).build());

        service.deleteTopic(topic.getId());

        service.getTopic(topic.getId());
    }

    @Test
    public void createTopic() throws Exception {
        Topic topic = service.createTopic(AddTopicDTO.builder().name("test").question("nameeee?").technologyId(1).build());

        assertNotNull(topic.getId());
    }

    @Test
    public void createAnswer() throws Exception {
        Topic topic = service.addAnswer(1, AnswerDTO.builder().answer("aaaaaaaww").points(0).build());
        assertNotNull(topic.getId());
    }

}