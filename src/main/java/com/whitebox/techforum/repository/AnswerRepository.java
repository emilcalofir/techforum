package com.whitebox.techforum.repository;

import com.whitebox.techforum.model.Answer;
import com.whitebox.techforum.model.dto.AnswerDTO;
import com.whitebox.techforum.model.dto.MyAnswersDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by emil on 9/4/2018.
 */
@RepositoryRestResource(exported = false)
public interface AnswerRepository extends PagingAndSortingRepository<Answer, Integer> {

    @Query("Select new com.whitebox.techforum.model.dto.AnswerDTO(o.answer, o.points, o.id) from Answer o where o.replyTo.id = :replyTo")
    Iterable<AnswerDTO> findMyReplies(@Param("replyTo")Integer replyTo);

    @Query("Select new com.whitebox.techforum.model.dto.AnswerDTO(o.answer, o.points, o.id) from Answer o where o.topicId = :topicId")
    Iterable<AnswerDTO> findAllByTopicId(@Param("topicId")Integer topicId);

    @Modifying
    @Transactional
    @Query("delete from Answer p where p.topicId = :topicId")
    void deleteByTopicId(@Param("topicId") Integer topicId);

    @Query("Select new com.whitebox.techforum.model.dto.AnswerDTO(o.answer, o.points, o.id) from Answer o where o.createdBy = :createdBy")
    List<AnswerDTO> findByCreatedBy(@Param("createdBy") Integer createdBy, Pageable pageable);

    @Query("Select new com.whitebox.techforum.model.dto.AnswerDTO(o.answer, o.points, o.id) from Answer o where LOWER(o.answer) like lower(concat('%', :query,'%')) AND o.createdBy = :createdBy")
    List<AnswerDTO> findByAnswer(@Param("query") String query, @Param("createdBy") Integer internalUserId, Pageable pageable);

    @Query("Select new com.whitebox.techforum.model.dto.MyAnswersDTO(a.answer, t.name, th.name) " +
            "from Technology th " +
            "JOIN th.topics t " +
            "JOIN t.answers a " +
            "where LOWER(a.answer) like lower(concat('%', :query,'%')) AND a.createdBy = :createdBy")
    List<MyAnswersDTO> findMyMy(@Param("query") String query, @Param("createdBy") Integer internalUserId);
}
