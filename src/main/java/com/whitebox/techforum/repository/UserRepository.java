package com.whitebox.techforum.repository;

import com.whitebox.techforum.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by emil on 9/10/2018.
 */
@RepositoryRestResource(exported = false)
public interface UserRepository extends JpaRepository<User, Integer> {

    User findByUsername(String username);
}