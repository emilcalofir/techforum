package com.whitebox.techforum.repository;

import com.whitebox.techforum.model.Category;
import com.whitebox.techforum.model.dto.CategoryProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

/**
 * Created by emil on 9/4/2018.
 */
@RepositoryRestResource()
public interface CategoryRepository extends PagingAndSortingRepository<Category, Integer> {

    @RestResource(exported = false)
    @Query("select c as category, count(t) as quantity from Category c join c.technologies t where c.id = ?1 group by c")
    CategoryProjection getDto(Integer categoryId);

    @RestResource(exported = false)
    @Query("select c as category, count(t) as quantity from Category c join c.technologies t group by c")
    List<CategoryProjection> getDtos();

    @RestResource(exported = false)
    @Query("select c as category, count(t) as quantity from Category c join c.technologies t group by c")
    Page<CategoryProjection> getDtos(Pageable pageable);
}
