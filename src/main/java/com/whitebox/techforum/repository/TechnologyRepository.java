package com.whitebox.techforum.repository;

import com.whitebox.techforum.model.Technology;
import com.whitebox.techforum.model.dto.TechnologyDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by emil on 9/4/2018.
 */
@RepositoryRestResource(exported = false)
public interface TechnologyRepository extends PagingAndSortingRepository<Technology, Integer> {

    @Query("Select new com.whitebox.techforum.model.dto.TechnologyDTO(o.id, o.name) from Technology o where o.categoryId = :categoryId")
    Iterable<TechnologyDTO> findAllByCategoryId(@Param("categoryId") Integer categoryId);

}
