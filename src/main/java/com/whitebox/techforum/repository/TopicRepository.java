package com.whitebox.techforum.repository;

import com.whitebox.techforum.model.Topic;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by emil on 9/4/2018.
 */
@RepositoryRestResource(exported = false)
public interface TopicRepository extends PagingAndSortingRepository<Topic, Integer> {

}
