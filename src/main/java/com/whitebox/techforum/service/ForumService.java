package com.whitebox.techforum.service;

import com.whitebox.techforum.exception.*;
import com.whitebox.techforum.model.Answer;
import com.whitebox.techforum.model.Technology;
import com.whitebox.techforum.model.Topic;
import com.whitebox.techforum.model.dto.*;
import com.whitebox.techforum.repository.AnswerRepository;
import com.whitebox.techforum.repository.CategoryRepository;
import com.whitebox.techforum.repository.TechnologyRepository;
import com.whitebox.techforum.repository.TopicRepository;
import com.whitebox.techforum.security.MyUserPrincipal;
import com.whitebox.techforum.util.Assert;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

/**
 * Created by emil on 9/6/2018.
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class ForumService {

    final TechnologyRepository technologyRepository;
    final CategoryRepository categoryRepository;
    final AnswerRepository answerRepository;
    final TopicRepository topicRepository;
    final EntityManager entityManager;

    public Iterable<AnswerDTO> getAnswersForTopics(Integer topicId) {
        return answerRepository.findAllByTopicId(topicId);
    }

    public Iterable<TechnologyDTO> getTechnologiesForCategory(Integer categoryId) {
        Iterable<TechnologyDTO> list = technologyRepository.findAllByCategoryId(categoryId);
        return list;
    }

    public Topic getTopic(Integer topicId) throws TopicNotFoundException {
        Optional<Topic> topic = topicRepository.findById(topicId);
        if(!topic.isPresent())
            throw new TopicNotFoundException(topicId);

        return topic.get();
    }

    @Transactional
    public void deleteTopic(Integer topicId) throws TopicNotFoundException {
        log.debug("Delete topic by id {}", topicId);
        try {
            answerRepository.deleteByTopicId(topicId);
            topicRepository.deleteById(topicId);
        }catch (EmptyResultDataAccessException e){
            throw new TopicNotFoundException(topicId);
        }
    }

    public Topic createTopic(AddTopicDTO topic) throws CreateTopicException {
        Topic newTopic;
        try {
            newTopic = Topic.builder().name(topic.getName()).question(topic.getQuestion()).technologyId(topic.getTechnologyId()).build();
            topicRepository.save(newTopic);
        }catch (Exception e){
            log.error("Cannot create topic {}",topic);
            throw new CreateTopicException(e.getCause());
        }

        return newTopic;
    }

    public Optional<Topic> moveTopic(MoveTopicDTO topic, Integer topicId) throws MoveTopicException, TechnologyNotFoundException, TopicNotFoundException {

        Optional<Technology> technology = technologyRepository.findById(topic.getTechnologyId());
        Optional<Topic> topic1 = topicRepository.findById(topicId);

        if (!technology.isPresent())
            throw new TechnologyNotFoundException(topic.getTechnologyId());

        if(!topic1.isPresent())
            throw new TopicNotFoundException(topicId);

        topic1.get().setTechnologyId(technology.get().getId());

        try {
            topicRepository.save(topic1.get());
        }catch (Exception e){
            throw new MoveTopicException(topic, e);
        }

        return topic1;
    }

    public Optional<Topic> updateTopic(EditTopicDTO topic, Integer topicId) throws Exception {
        Optional<Topic> topic1 = topicRepository.findById(topicId);
        Assert.hasValue(topic1, new TopicNotFoundException(topicId));

        if(topic.getName() != null)
            topic1.get().setName(topic.getName());

        if(topic.getQuestion() != null)
            topic1.get().setQuestion(topic.getQuestion());

        try {
            topicRepository.save(topic1.get());
        }catch (Exception e){
            throw new EditTopicException(topic, e);
        }

        return topic1;
    }

    @Transactional
    public Topic addAnswer(Integer topicId, AnswerDTO topicDTO) throws Exception {
        Optional<Topic> topic = topicRepository.findById(topicId);
        Assert.hasValue(topic, new TopicNotFoundException(topicId));

        Answer answer = Answer.builder()
                .answer(topicDTO.getAnswer())
                .points(topicDTO.getPoints())
                .acceptedAnswer(true)
                .createdBy(getInternalUserId())
                .topicId(topicId).build();

        answerRepository.save(answer);

        topic.get().addAnswer(answer);

        entityManager.flush();

        return topic.get();
    }

    private Integer getInternalUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        MyUserPrincipal user =  (MyUserPrincipal)authentication.getPrincipal();
        return user.getInternalUserId();
    }

    @Transactional
    public Answer vote(Integer answerId) throws Exception {
        Optional<Answer> answer = answerRepository.findById(answerId);
        Assert.hasValue(answer, new AnswerNotFoundException(answerId));
        answer.get().vote();
        answerRepository.save(answer.get());

        return answer.get();
    }

    public List<AnswerDTO> getMyActivity(Integer page, Integer size) {
        return answerRepository.findByCreatedBy(getInternalUserId(), PageRequest.of(page, size, Sort.by("points")));
    }

    public List<AnswerDTO> search(String query, Integer page, Integer size) {
        return answerRepository.findByAnswer(query, getInternalUserId(), PageRequest.of(page, size, Sort.by("points")));
    }
}
