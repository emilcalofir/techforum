package com.whitebox.techforum.model;

import lombok.*;
import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import java.util.List;

/**
 * Created by emil on 9/4/2018.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Entity
@ToString(exclude = {"technologies"})
public class Category implements Identifiable<Integer> {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="category_seq")
    @SequenceGenerator(name="category_seq", sequenceName = "category_seq")
    Integer id;

    String name;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn( name = "category_id", referencedColumnName = "id")
    List<Technology> technologies;

}
