package com.whitebox.techforum.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by emil on 9/9/2018.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EditTopicDTO {
    String name;
    String question;
}
