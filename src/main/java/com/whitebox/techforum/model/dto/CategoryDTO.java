package com.whitebox.techforum.model.dto;

import com.whitebox.techforum.model.Category;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.core.Relation;

/**
 * Created by emil on 9/6/2018.
 */
@Data
@Builder
@RequiredArgsConstructor
@Relation(value = "category", collectionRelation = "categories")
public class CategoryDTO implements CategoryProjection {
    final private Category category;
    final private Integer quantity;
}
