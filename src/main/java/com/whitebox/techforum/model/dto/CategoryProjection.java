package com.whitebox.techforum.model.dto;

import com.whitebox.techforum.model.Category;

/**
 * Created by emil on 10/6/2018.
 */
public interface CategoryProjection {
    Category getCategory();
    Integer getQuantity();
}
