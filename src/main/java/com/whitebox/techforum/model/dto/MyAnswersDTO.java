package com.whitebox.techforum.model.dto;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * Created by emil on 10/7/2018.
 */
@Data
@Builder
@RequiredArgsConstructor
public class MyAnswersDTO implements MyAnswersProjection {

    final String answerName;
    final String topicName;
    final String technologyName;
}
