package com.whitebox.techforum.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by emil on 9/6/2018.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AnswerDTO {
    String answer;
    Integer points;

    //tmp
    Integer id;
}
