package com.whitebox.techforum.model.dto;

/**
 * Created by emil on 10/7/2018.
 */
public interface MyAnswersProjection {
    String getAnswerName();
    String getTopicName();
    String getTechnologyName();
}
