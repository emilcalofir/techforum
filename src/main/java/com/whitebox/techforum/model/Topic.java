package com.whitebox.techforum.model;

import lombok.*;
import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emil on 9/4/2018.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"answers"})
@Entity
public class Topic implements Identifiable<Integer> {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="topic_seq")
    @SequenceGenerator(name="topic_seq", sequenceName = "topic_seq")
    Integer id;

    String name;

    String question;

    @Column(name = "technology_id", nullable = false)
    Integer technologyId;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinColumn( name = "topic_id", referencedColumnName = "id" )
    List<Answer> answers;

    public void addAnswer(Answer answer) {
        if(answers == null){
            answers = new ArrayList<>();
        }

        answer.setTopicId(this.id);
        answers.add(answer);
    }
}
