package com.whitebox.techforum.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.Identifiable;

import javax.persistence.*;

/**
 * Created by emil on 9/4/2018.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Entity
public class Answer implements Identifiable<Integer> {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="answer_seq")
    @SequenceGenerator(name="answer_seq", sequenceName = "answer_seq")
    Integer id;

    String answer;

    @Column(name = "accepted_answer")
    Boolean acceptedAnswer;

    Integer points;

    @Column(name = "topic_id", nullable = false)
    Integer topicId;

    @Column(name = "reply_to")
    Integer replyTo;

    @Column(name = "created_by", nullable = false)
    Integer createdBy;


    public void vote() {
        if(points == null)
            points = new Integer(0);

        points++;
    }
}
