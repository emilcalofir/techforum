package com.whitebox.techforum.model;

import lombok.*;
import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import java.util.List;

/**
 * Created by emil on 9/4/2018.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"topics"})
@Entity
public class Technology implements Identifiable<Integer> {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="technology_seq")
    @SequenceGenerator(name="technology_seq", sequenceName = "technology_seq")
    Integer id;

    String name;

    @Column(name = "category_id", nullable = false)
    Integer categoryId;


    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn( name = "technology_id", referencedColumnName = "id" )
    List<Topic> topics;
}
