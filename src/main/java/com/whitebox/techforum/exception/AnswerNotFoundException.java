package com.whitebox.techforum.exception;

/**
 * Created by emil on 9/12/2018.
 */
public class AnswerNotFoundException extends Exception {
    public AnswerNotFoundException(Integer answerId) {
        super("Answer not found with id "+answerId);
    }
}
