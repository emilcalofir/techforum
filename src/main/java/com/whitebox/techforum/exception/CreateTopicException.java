package com.whitebox.techforum.exception;

/**
 * Created by emil on 9/9/2018.
 */
public class CreateTopicException extends Exception{

    public CreateTopicException(Throwable e) {
        super("Could not create topic", e);
    }
}
