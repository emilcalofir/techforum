package com.whitebox.techforum.exception;

import com.whitebox.techforum.model.dto.MoveTopicDTO;

/**
 * Created by emil on 9/9/2018.
 */
public class MoveTopicException extends Throwable {
    public MoveTopicException(MoveTopicDTO topic, Exception e) {
        super("Could not move topic "+topic, e);
    }
}
