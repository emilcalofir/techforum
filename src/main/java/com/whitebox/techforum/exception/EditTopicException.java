package com.whitebox.techforum.exception;

import com.whitebox.techforum.model.dto.EditTopicDTO;

/**
 * Created by emil on 9/9/2018.
 */
public class EditTopicException extends Exception {
    public EditTopicException(EditTopicDTO topic, Exception e) {
        super("Could not edit topic "+topic, e);
    }
}
