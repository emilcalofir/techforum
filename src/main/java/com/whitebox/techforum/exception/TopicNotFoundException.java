package com.whitebox.techforum.exception;

/**
 * Created by emil on 9/9/2018.
 */
public class TopicNotFoundException extends Exception {

    public TopicNotFoundException(Integer topicId) {
        super("Topic id "+topicId+" was not found");
    }
}
