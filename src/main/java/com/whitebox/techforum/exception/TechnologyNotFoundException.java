package com.whitebox.techforum.exception;

/**
 * Created by emil on 9/9/2018.
 */
public class TechnologyNotFoundException extends Exception {

    public TechnologyNotFoundException(Integer technologyId) {
        super("Technology with id "+technologyId+" was not found");
    }
}
