package com.whitebox.techforum.config;

import org.hsqldb.server.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by emil on 9/4/2018.
 */
@Configuration
public class DatabaseConfiguration {

    @Bean
    public Server server() {
        String myDataBase = "mydatabase";

        Server server = new Server();
        server.setDatabaseName(0, myDataBase);
        server.setDatabasePath(0, "file:" + myDataBase + "db");
        server.start();

        return server;
    }
}
