package com.whitebox.techforum.application;

import com.whitebox.techforum.exception.CreateTopicException;
import com.whitebox.techforum.exception.MoveTopicException;
import com.whitebox.techforum.exception.TechnologyNotFoundException;
import com.whitebox.techforum.exception.TopicNotFoundException;
import com.whitebox.techforum.model.Answer;
import com.whitebox.techforum.model.Topic;
import com.whitebox.techforum.model.dto.*;
import com.whitebox.techforum.service.ForumService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

/**
 * Created by emil on 9/7/2018.
 */
@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class ForumController {

    @NonNull
    private final ForumService forumService;
/*
    @GetMapping("/categories/{id}")
    public CategoryDTO fetchCategory(@PathVariable("id") Integer categoryId){
        log.info("call to get category by id, {}", categoryId);
        return forumService.getCategory(categoryId);
    }*/

    @GetMapping("/categories/{id}/technologies")
    public Iterable<TechnologyDTO> fetchTechnologies(@PathVariable("id") Integer categoryId){
        log.info("call to get list of technologies by category {}", categoryId);
        return forumService.getTechnologiesForCategory(categoryId);
    }

    @GetMapping("/topics/{id}")
    public Topic fetchTopic(@PathVariable("id") Integer topicId) throws TopicNotFoundException {
        log.info("call to get {}", topicId);
        return forumService.getTopic(topicId);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("/topics/{id}")
    public void deleteTopic(@PathVariable("id") Integer topicId) throws TopicNotFoundException {
        log.info("call to delete topic {}", topicId);
        forumService.deleteTopic(topicId);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/topics")
    public ResponseEntity<Object> createTopic(@RequestBody AddTopicDTO topic) throws CreateTopicException {

        log.info("call to create topic {}", topic);
        Topic createdTopic = forumService.createTopic(topic);
        log.info("A topic was created with id {}", createdTopic.getId());

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(createdTopic.getId()).toUri();

        return  ResponseEntity.created(location).build();
    }

    @PutMapping("/topics/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> moveTopic(@RequestBody MoveTopicDTO topic, @PathVariable("id") Integer topicId) throws MoveTopicException, TopicNotFoundException, TechnologyNotFoundException {
        log.info("call to update topic {} move to {}", topicId, topic);
        Optional<Topic> topicOptional = forumService.moveTopic(topic, topicId);

        if (!topicOptional.isPresent())
            return ResponseEntity.notFound().build();

        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/topics/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> editTopic(@RequestBody EditTopicDTO topic, @PathVariable("id") Integer topicId) throws Exception {
        log.info("call to edit topic {} with {}", topicId, topic);
        Optional<Topic> topicOptional = forumService.updateTopic(topic, topicId);

        if (!topicOptional.isPresent())
            return ResponseEntity.notFound().build();

        return ResponseEntity.noContent().build();
    }


    @GetMapping("/topics/{id}/answers")
    public Iterable<AnswerDTO> fetchAnswers(@PathVariable("id") Integer topicId){
        log.info("call to get list of answers for topic {}", topicId);
        return forumService.getAnswersForTopics(topicId);
    }

    @PostMapping("/topics/{id}/answers")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<Object> answerQuestion(@RequestBody AnswerDTO answerDTO, @PathVariable("id") Integer topicId) throws Exception {
        log.info("call to update topic {} move to {}", topicId, answerDTO);
        forumService.addAnswer(topicId, answerDTO);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/answers/{id}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<Object> vote(@PathVariable("id") Integer answerId) throws Exception{
        log.info("call to vote answer {} ", answerId);
        Answer answer = forumService.vote(answerId);
        return ResponseEntity.ok(answer.getPoints());
    }

    @GetMapping(value = "/account/activity", params = { "page", "size" })
    @PreAuthorize("hasRole('ROLE_USER')")
    public List<AnswerDTO> myActivity(
            @RequestParam( "page" ) Integer page, @RequestParam( "size" ) Integer size) throws Exception{
        log.info("Call get my activity");
        List<AnswerDTO> answer = forumService.getMyActivity(page, size);
        return answer;
    }


    @GetMapping(value = "/account/search", params = { "q", "page", "size" })
    @PreAuthorize("hasRole('ROLE_USER')")
    public List<AnswerDTO> search(
            @RequestParam( "q" ) String query, @RequestParam( "page" ) Integer page, @RequestParam( "size" ) Integer size) throws Exception{
        log.info("Call search my answers");
        List<AnswerDTO> answer = forumService.search(query, page, size);
        return answer;
    }


}
