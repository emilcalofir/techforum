package com.whitebox.techforum.application;

import com.whitebox.techforum.model.dto.CategoryDTO;
import com.whitebox.techforum.model.dto.CategoryProjection;
import com.whitebox.techforum.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by emil on 10/6/2018.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/categories")
public class CategoryController {

    final CategoryRepository categoryRepository;
    final RepositoryEntityLinks links;
    final PagedResourcesAssembler<CategoryProjection> assembler;

    /**
     * Single DTO
     */
    @GetMapping("/{id}/dto")
    public ResponseEntity<?> getDto(@PathVariable("id") Integer categoryId) {
        CategoryProjection dto = categoryRepository.getDto(categoryId);

        return ResponseEntity.ok(toResource(dto));
    }


    private ResourceSupport toResource(CategoryProjection projection) {
        CategoryDTO dto = CategoryDTO.builder().category(projection.getCategory()).quantity(projection.getQuantity()).build();

        Link categoryLink = links.linkForSingleResource(projection.getCategory()).withRel("category");

        //Link categoryLink = links.linkForSingleResource(projection.getCategory()).withRel("category");
        //Link selfLink = links.linkForSingleResource(projection.getCategory()).slash("/dto").withSelfRel();

        return new Resource<>(dto, categoryLink);
    }

}
