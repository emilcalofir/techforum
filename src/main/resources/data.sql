
INSERT INTO CATEGORY (id, name)
VALUES
(1, 'Programming Languages'),
(2, 'Frameworks'),
(3,  'Continuous Deployment Technologies'),
(4, 'Cache management')
;


INSERT INTO TECHNOLOGY (id, name, category_id)
VALUES
(1, 'Java', 1),
(2, 'Kotlin', 1),
(3, 'TypeScript', 1),
(4, 'C++', 1),

(5, 'Spring', 2),
(6, 'Angular', 2),
(7, 'React', 2),

(8, 'Rancher', 3),
(9, 'Docker', 3),
(10, 'Kubernetes', 3),
(11, 'TeamCity', 3),

(12, 'EhCache', 4),
(13, 'Redis', 4),
(14, 'Memcache', 4),
(15, 'Varnish', 4)
;


INSERT INTO TOPIC (id, name, question, technology_id)
VALUES
(1, 'What is useful in Java 10?', 'From my experience the Java 9 was ....', 1),
(2, '“How should I use Optional correctly?', 'this is my way of using the Optional ....', 1)
;

INSERT INTO ANSWER (id, answer, points, accepted_answer, topic_id, created_by, reply_to)
VALUES
(1, 'My opinion is taht..?', 1, 0, 1, 2, null),
(2, 'Idontk think.........', 5, 0, 1, 3, null),
(3, 'Youm may be right....', 5, 0, 1, 2, 1),
(4, 'Yes you areright.....', 0, 0, 1, 3, 2),
(5, 'Yes optional correc..', 3, 0, 1, 2, null),
(6, 'Yes is my way o.j7...', 5, 0, 1, 2, null)
;


INSERT INTO TECHUSER (id, username, password, role_id)
VALUES
(1, 'admin@tech', '$2a$04$EGY8VZlma0VdrJKNiKmqlOOMUw0eFRgxjrqPS0WOCWsiVleFSdDkO', 1), --p: pass1
(2, 'user1@tech', '$2a$04$O8a/q75Wmkw.V0cPImkcZOPDrLQmusutlhVvFwqa3PtTrxKv9Hs7O', 2), --p: pass2
(3, 'user2@tech', '$2a$04$XiDYnJXC/u9.7ybmW.DbIOess49fEaq2mlZAt41..5/g1wQHZiy2C', 2)  --p: pass3
;

INSERT INTO TECHROLE (id, name)
VALUES
(1, 'ADMIN'),
(2, 'USER')
;

