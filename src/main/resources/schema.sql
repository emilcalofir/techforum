


create sequence IF NOT EXISTS category_seq START WITH 100 INCREMENT BY 1;

CREATE TABLE IF NOT EXISTS CATEGORY (
	id                  INTEGER  PRIMARY KEY,
	name                VARCHAR(100)
);


create sequence IF NOT EXISTS technology_seq START WITH 100 INCREMENT BY 1;
CREATE TABLE IF NOT EXISTS TECHNOLOGY (
	id                  INTEGER PRIMARY KEY,
	name                VARCHAR(100),
	category_id          INTEGER NOT NULL,

	foreign key (category_id) references CATEGORY(id)
);


create sequence IF NOT EXISTS topic_seq START WITH 100 INCREMENT BY 1;
CREATE TABLE IF NOT EXISTS TOPIC (
	id                  INTEGER PRIMARY KEY,
	name                VARCHAR(255),

	question            VARCHAR(1024) NOT NULL,
	technology_id        INTEGER NOT NULL,


	foreign key (technology_id) references TECHNOLOGY(id)
);

create sequence IF NOT EXISTS answer_seq START WITH 100 INCREMENT BY 1;
CREATE TABLE IF NOT EXISTS ANSWER (
	id                  INTEGER PRIMARY KEY,
	answer              VARCHAR(100) NOT NULL,

	points              INTEGER,
	accepted_answer      BOOLEAN,

	topic_id             INTEGER NOT NULL,
	created_by           INTEGER NOT NULL,
	reply_to             INTEGER,

	foreign key (topic_id) references TOPIC(id),
	foreign key (created_by) references TECHUSER(id),
	foreign key (reply_to) references ANSWER(id)
);

CREATE TABLE IF NOT EXISTS TECHUSER (
	id                  INTEGER PRIMARY KEY,

	username           VARCHAR(100) NOT NULL,
	password           VARCHAR(100) NOT NULL,
	role_id            INTEGER NOT NULL,

	foreign key (role_id) references TECHROLE(id)
);


CREATE TABLE IF NOT EXISTS TECHROLE (
	id                  INTEGER PRIMARY KEY,

	name              VARCHAR(100) NOT NULL
);




